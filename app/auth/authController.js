(function() {

	angular.module('primeiraApp').controller('AuthCtrl', [
		'$location',
		'msgs',
		'auth',
		AuthController
	])
	.directive('googleSignInButton', function() {
		return {
			scope: {
				buttonId: '@',
				options: '&'
			},
			template: '<div id="{{ buttonId }}"></div>',
			controller: 'AuthCtrl',
			controllerAs: 'auth',
			link: function(scope, element, attrs, $auth) {
				var div = element.find('div')[0];
				div.id = attrs.buttonId;
				gapi.signin2.render(attrs.buttonId, $auth.options); //render a google button, first argument is an id, second options
			}
		};
	})

	function AuthController($location, msgs, auth) {
		const vm = this

		vm.loginMode = true
		vm.options = {
			scope: 'email',
			//width: 200,
			//height: 50,
			longtitle: false,
			theme: 'ligth',
			onfailure: function(response) {
              console.log(response);
            },
			'onsuccess': function(response) {
              console.log(response);
            }
		}

		vm.changeMode = () => vm.loginMode = !vm.loginMode

		vm.login = () => {
			auth.login(vm.user, err => err ? msgs.addError(err) : $location.path('/'))
		}

		vm.signup = () => {
			auth.signup(vm.user, err => err ? msgs.addError(err) : $location.path('/'))
		}

		vm.getUser = () => auth.getUser()

		vm.logout = () => {
			auth.logout(() => $location.path('/'))
		}
	}

})()